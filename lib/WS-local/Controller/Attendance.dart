import 'package:sac/Inc/Inc.dart';
import 'package:sac/WS-local/Config/WsLocal.dart';
import 'package:sac/WS-local/Model/AttendanceDetailsJsonModel.dart';

class Attendance{
  WsLocal wsLocal;
  _Send send;
  _Subscribe subscribe;
  Attendance(this.wsLocal){
    send = _Send(wsLocal);
    subscribe = _Subscribe(wsLocal);
    _var = _Var();
  }

  start(){
    subscribe.login();
    subscribe.allClasses();
    subscribe.allSections();
    subscribe.allStudents();
    subscribe.addAttendances();
  }

  stop(){
    subscribe.unSubLoginRespon();
    subscribe.unSubLoginRespon = null;

    subscribe.unSubAllClassesRespon();
    subscribe.unSubAllClassesRespon = null;

    subscribe.unSubAllSectionsRespon();
    subscribe.unSubAllSectionsRespon = null;

    subscribe.unSubAllStudentsRespon();
    subscribe.unSubAllStudentsRespon = null;

    subscribe.unSubAddAttendancesRespon();
    subscribe.unSubAddAttendancesRespon = null;
  }


}

_Var _var = null;

class _Var{

  Function Function(String data) onGetClasses = null;

  Function Function(String data) onGetSections = null;

  Function Function(String data) onGetStudents = null;

  Function Function() closeAfterSend = null;
}

class _Send{
  WsLocal wsLocal;
  _Send(this.wsLocal);

  login(String password,Function onGetClasses(String data)){
    _var.onGetClasses = onGetClasses;
    wsLocal.ws.send(destination: "/s/login",body: password);
  }

  allClasses(){
    wsLocal.ws.send(destination: "/s/allClasses",body: null);
  }

  allSections(int class_id, Function onGetSections(String data)){
      _var.onGetSections = onGetSections;
      wsLocal.ws.send(destination: "/s/allSections",body: class_id.toString());
  }

  allStudents(String json, Function onGetStudents(String data)){
      _var.onGetStudents = onGetStudents;
      wsLocal.ws.send(destination: "/s/allStudents",body: json);
  }

  addAttendances(AttendanceDetailsJsonModel attendanceDetailsJsonModel,Function closeAfterSend){
    _var.closeAfterSend = closeAfterSend;
    wsLocal.ws.send(destination: "/s/addAttendances",body: attendanceDetailsJsonModel.toJSON());
  }
}

class _Subscribe{
  WsLocal wsLocal;
  _Subscribe(this.wsLocal);

  dynamic unSubLoginRespon = null;
  login(){
    unSubLoginRespon != null ? {unSubLoginRespon()}:"";
    unSubLoginRespon = null;
    unSubLoginRespon = this.wsLocal.ws.subscribe(destination: "/c/login", callback: (stompFrame){
      if(stompFrame.body == "true"){
        wsLocal.attendance.send.allClasses();
      }else{
        Inc.ws.disconnect();
        Inc.ws.serverPasswordError();

      }
    });
  }

  dynamic unSubAllClassesRespon = null;
  allClasses(){
    unSubAllClassesRespon != null ? {unSubAllClassesRespon()}:"";
    unSubAllClassesRespon = null;
    unSubAllClassesRespon = this.wsLocal.ws.subscribe(destination: "/c/allClasses", callback: (stompFrame){
      if(stompFrame.body == "true" || stompFrame.body == "false"){
      }else{
        if(_var.onGetClasses != null){
          _var.onGetClasses(stompFrame.body);
          _var.onGetClasses = null;
        }
      }
    });
  }

  dynamic unSubAllSectionsRespon = null;
  allSections(){
    unSubAllSectionsRespon != null ? {unSubAllSectionsRespon()}:"";
    unSubAllSectionsRespon = null;
    unSubAllSectionsRespon = this.wsLocal.ws.subscribe(destination: "/c/allSections", callback: (stompFrame){
        if(stompFrame.body == "true" || stompFrame.body == "false"){
        }else{
          if(_var.onGetSections != null){
            _var.onGetSections(stompFrame.body);
            _var.onGetSections = null;
          }
        }
    });
  }

  dynamic unSubAllStudentsRespon = null;
  allStudents(){
    unSubAllStudentsRespon != null ? {unSubAllStudentsRespon()}:"";
    unSubAllStudentsRespon = null;
    unSubAllStudentsRespon = this.wsLocal.ws.subscribe(destination: "/c/allStudents", callback: (stompFrame){
        if(stompFrame.body == "password_error" || stompFrame.body == "section_id_error"){
        }else{
          if(_var.onGetStudents != null){
            _var.onGetStudents(stompFrame.body);
            _var.onGetStudents = null;
          }
        }
    });
  }

  dynamic unSubAddAttendancesRespon = null;
  addAttendances(){
    unSubAddAttendancesRespon != null ? {unSubAddAttendancesRespon()}:"";
    unSubAddAttendancesRespon = null;
    unSubAddAttendancesRespon = this.wsLocal.ws.subscribe(destination: "/c/addAttendances", callback: (stompFrame){
        if(stompFrame.body == "true"){
          if(_var.closeAfterSend != null){
            _var.closeAfterSend();
          }
        }else{
        }
    });
  }
}