

import 'dart:convert';

import 'package:sac/WS-local/Model/ClassesJsonModel.dart';
import 'package:sac/WS-local/Model/SectionJsonModel.dart';

class AttendanceDetailsJsonModel{
  ClassesJsonModel classesModel = null;
  SectionJsonModel sectionModel = null;
  String date = null;
  String time = null;
  List<int> arrPresentStudents_ids = List();
  List<int> arrLateStudents_ids = List();
  List<int> arrAbsenStudents_ids = List();

  AttendanceDetailsJsonModel(
      this.classesModel,
      this.sectionModel,
      this.date,
      this.time,
      this.arrPresentStudents_ids,
      this.arrLateStudents_ids,
      this.arrAbsenStudents_ids);

  Map<String, dynamic> toMap() =>
      {
        'classesModel': classesModel.toMap(),
        'sectionModel': sectionModel.toMap(),
        'date': date,
        'time': time,
        'arrPresentStudents_ids': arrPresentStudents_ids,
        'arrLateStudents_ids': arrLateStudents_ids,
        'arrAbsenStudents_ids': arrAbsenStudents_ids,
      };
  String toJSON(){
    return json.encode(toMap());
  }
}