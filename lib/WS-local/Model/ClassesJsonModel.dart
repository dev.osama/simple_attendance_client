

import 'dart:convert';

class ClassesJsonModel{
  int class_id;
  String class_name;
  ClassesJsonModel(var data) {
    this.class_id = data['class_id'];
    this.class_name = data['class_name'];
  }
  Map<String, dynamic> toMap() =>
      {
        'class_id': class_id,
        'class_name': class_name,
      };
  String toJSON(){
    return jsonEncode(toMap());
  }
}