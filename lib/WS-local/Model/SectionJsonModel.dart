import 'dart:convert';

class SectionJsonModel{
  int section_id;
  String section_name;
  String section_password;
  int class_id;

  SectionJsonModel(var data) {
    this.section_id = data['section_id'];
    this.section_name = data['section_name'];
    this.section_password = data['section_password'];
    this.class_id = data['class_id'];
  }
  Map<String, dynamic> toMap() =>
      {
        'section_id': section_id,
        'section_name': section_name,
        'section_password': section_password,
        'class_id': class_id,
      };
  String toJSON(){
    return json.encode(toMap());
  }
}