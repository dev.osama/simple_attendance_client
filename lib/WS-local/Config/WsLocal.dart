
import 'package:sac/WS-local/Controller/Attendance.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

class WsLocal{
  String HOST_NAME = "192.168.1.4";
  String PORT_NUMB = "8988";
  StompClient ws;

  Attendance attendance;

  WsLocal(){
    ws = StompClient(
        config: StompConfig(
          url: 'ws://$HOST_NAME:$PORT_NUMB/ws/',
          onConnect: _onConnectCallback,
          onDisconnect: _onDisconnectCallback,
          heartbeatIncoming: 10000,
          heartbeatOutgoing: 10000,
        )
    );
    attendance = Attendance(this);
  }

  connect(){
    ws.activate();
  }

  connectWithIp(String ip,int port,String password,Function onConnectAndGetClasses,Function serverPasswordError){
    afterSubscribe = onConnectAndGetClasses;
    this.serverPasswordError = serverPasswordError;
    this.password = password;
    ws = StompClient(
        config: StompConfig(
          url: 'ws://$ip:$port/ws/',
          onConnect: _onConnectCallback,
          onDisconnect: _onDisconnectCallback,
          onWebSocketDone: _onWebSocketDone,
          onStompError: _onStompError,
          onWebSocketError: _onWebSocketError,
          heartbeatIncoming: 10000,
          heartbeatOutgoing: 10000,
        )
    );
    ws.activate();
  }


  _onConnectCallback(StompClient client, StompFrame connectFrame){
    attendance.start();
    attendance.send.login(password, afterSubscribe);

  }
  String password = null;
  Function afterSubscribe = null;
  Function serverPasswordError = null;

  _onDisconnectCallback(StompFrame connectFrame){
  }

  _onStompError(StompFrame stompFrame) {
  }

  _onWebSocketError(p1) {
  }

  _onWebSocketDone(){
  }

  void disconnect() {
    attendance.stop();
    ws.deactivate();
  }
}