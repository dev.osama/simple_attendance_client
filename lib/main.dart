import 'package:flutter/material.dart';
import 'package:sac/Inc/Inc.dart';

import 'Screen/Connection.dart';

void main() {
  Inc.start();
  runApp(
    MaterialApp(
      home: Connection(),
      debugShowCheckedModeBanner: false,
    ),
  );
}

