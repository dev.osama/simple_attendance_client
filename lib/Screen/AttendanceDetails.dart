// import 'dart:html';

import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as AAA;
import 'package:sac/Inc/Inc.dart';
import 'package:sac/Screen/Connection.dart';
import 'package:sac/WS-local/Model/AttendanceDetailsJsonModel.dart';
import 'package:sac/WS-local/Model/ClassesJsonModel.dart';
import 'package:sac/WS-local/Model/SectionJsonModel.dart';

class AttendanceDetails extends StatefulWidget {
  SectionJsonModel sectionJsonModel;
  ClassesJsonModel classesJsonMode;
  HashMap<int,int> finalAttendsArr;

  AttendanceDetails(this.classesJsonMode,this.sectionJsonModel,this.finalAttendsArr);
  @override
  State<StatefulWidget> createState() {
    return _AttendanceDetails(this.classesJsonMode,this.sectionJsonModel,this.finalAttendsArr);
  }
}

class _AttendanceDetails extends State<AttendanceDetails> {
  SectionJsonModel sectionJsonModel ;
  HashMap<int,int> finalAttendsArr ;
  ClassesJsonModel classesJsonMode ;
  AAA.DateFormat date = new AAA.DateFormat('yyyy/MM/dd');
  String dateString = "";
  AAA.DateFormat time = new AAA.DateFormat('HH:mm:ss');
  String timeString = "";

  //present = 1
  //late    = 2
  //absent  = 3
  List<int> arrPresentStudents_ids = List();
  List<int> arrLateStudents_ids = List();
  List<int> arrAbsenStudents_ids = List();


  _AttendanceDetails(this.classesJsonMode,this.sectionJsonModel,this.finalAttendsArr){
    finalAttendsArr.forEach((key, value) {
        switch(value){
          case 1://present
            arrPresentStudents_ids.add(key);
            break;
          case 2://late
            arrLateStudents_ids.add(key);
            break;
          case 3://absent
            arrAbsenStudents_ids.add(key);
            break;
          default:
            print("Hmmmmmmmm!!!!!!!!!");
        }
    });
    dateString = date.format(DateTime.now());
    timeString = time.format(DateTime.now());
  }
  yesNo_closeWS(){
    showDialog(
      context: context,
      builder:(BuildContext context){
        return AlertDialog(
          content: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Positioned(
                right: -40.0,
                top: -40.0,
                child: InkResponse(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: CircleAvatar(
                    child: Icon(Icons.close),
                    backgroundColor: Colors.red,
                  ),
                ),
              ),
              Form(
//              key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("اغلاق الاتصال"),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("اغلاق الاتصال",textDirection: TextDirection.rtl,),
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("نعم"),
                              onPressed: () {
                                Inc.ws.disconnect();
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => new Connection()), (route) => false);
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("لا"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    Function closeAfterSend = (){

      Inc.ws.disconnect();
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => new Connection()), (route) => false);
    };
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "الملخص",
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),
          ),
        ),
        actions: [
          InkWell(
            child: Icon(Icons.close),
            onTap: (){
              yesNo_closeWS();
            },
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ListView(
                children: [
                  ListTile(
                    title: Text(
                      "المادة",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      this.classesJsonMode.class_name,
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  ListTile(
                    title: Text(
                      "الشعبة",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      this.sectionJsonModel.section_name,
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  ListTile(
                    title: Text(
                      "عدد الحاضرين",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      arrPresentStudents_ids.length.toString(),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  ListTile(
                    title: Text(
                      "عدد المتأخرين",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      arrLateStudents_ids.length.toString(),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  ListTile(
                    title: Text(
                      "عدد الغائبين",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      arrAbsenStudents_ids.length.toString(),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  ListTile(
                    title: Text(
                      "التاريخ",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      dateString,
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  ListTile(
                    title: Text(
                      "الوقت",
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      timeString,
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {
                  Inc.ws.attendance.send.addAttendances(AttendanceDetailsJsonModel(classesJsonMode, sectionJsonModel, dateString, timeString, arrPresentStudents_ids, arrLateStudents_ids, arrAbsenStudents_ids),closeAfterSend);
                },
                child: Text(
                  "ارسال وانهاء الاتصال",
                  textDirection: TextDirection.rtl,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
