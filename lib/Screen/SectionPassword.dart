import 'package:flutter/material.dart';
import 'package:sac/Inc/Inc.dart';
import 'package:sac/Screen/Connection.dart';
import 'package:sac/Screen/Students.dart';
import 'package:sac/WS-local/Model/ClassesJsonModel.dart';
import 'package:sac/WS-local/Model/SectionJsonModel.dart';

class SectionPassword extends StatefulWidget {
  SectionJsonModel model = null;
  ClassesJsonModel classesJsonMode = null;
  SectionPassword(this.classesJsonMode,this.model);
  @override
  State<StatefulWidget> createState() {
    return _SectionPassword(this.classesJsonMode,this.model);
  }
}

class _SectionPassword extends State<SectionPassword> {
  SectionJsonModel model = null;
  ClassesJsonModel classesJsonMode = null;
  _SectionPassword(this.classesJsonMode,this.model);
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Function onGetStudents = (String data){
      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => new Students(this.classesJsonMode,this.model,data)),
      );
    };
    yesNo_closeWS(){
      showDialog(
        context: context,
        builder:(BuildContext context){
          return AlertDialog(
            content: Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Positioned(
                  right: -40.0,
                  top: -40.0,
                  child: InkResponse(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: CircleAvatar(
                      child: Icon(Icons.close),
                      backgroundColor: Colors.red,
                    ),
                  ),
                ),
                Form(
//              key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("اغلاق الاتصال"),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("اغلاق الاتصال",textDirection: TextDirection.rtl,),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                child: Text("نعم"),
                                onPressed: () {
                                  Inc.ws.disconnect();
                                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => new Connection()), (route) => false);
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                child: Text("لا"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      );
    }
    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              "كلمة مرور الشعبة",
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),
            ),
          ),
          actions: [
            InkWell(
              child: Icon(Icons.close),
              onTap: (){
                yesNo_closeWS();
              },
            ),
          ],
        ),
        body: Container(
          alignment: Alignment.center,
          child: ListView(
            children: [
              Text(
                "كلمة المرور الشعبة",
                textDirection: TextDirection.rtl,
              ),
              TextField(
                textDirection: TextDirection.ltr,
                textAlign: TextAlign.center,
                controller: password,
                obscureText: true,
              ),
              RaisedButton(
                onPressed: () {
                  this.model.section_password = password.text;
                  Inc.ws.attendance.send.allStudents(this.model.toJSON(),onGetStudents);
                },
                child: Text(
                  "ارسال",
                  textDirection: TextDirection.rtl,
                ),
              )
            ],
          ),
        ),
      );
  }
}
