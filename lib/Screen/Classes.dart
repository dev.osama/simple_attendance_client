import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sac/Inc/Inc.dart';
import 'package:sac/Screen/Connection.dart';
import 'package:sac/Screen/Section.dart';
import 'package:sac/WS-local/Model/ClassesJsonModel.dart';

class Classes extends StatefulWidget {
  String data = null;
  List<ClassesJsonModel> listArr = [];
  Classes(this.data){
    var jsonData = jsonDecode(data);
    for(var i in jsonData){
      listArr.add(ClassesJsonModel(i));
    }
  }
  @override
  State<StatefulWidget> createState() {
    return _Classes(this.listArr);
  }
}

class _Classes extends State<Classes> {
  List<ClassesJsonModel> listArr = null;
  _Classes(this.listArr);

  BuildContext context = null;
  yesNo_closeWS(){
    showDialog(
      context: context,
      builder:(BuildContext context){
        return AlertDialog(
          content: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Positioned(
                right: -40.0,
                top: -40.0,
                child: InkResponse(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: CircleAvatar(
                    child: Icon(Icons.close),
                    backgroundColor: Colors.red,
                  ),
                ),
              ),
              Form(
//              key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("اغلاق الاتصال"),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("اغلاق الاتصال",textDirection: TextDirection.rtl,),
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("نعم"),
                              onPressed: () {
                                Inc.ws.disconnect();
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => new Connection()), (route) => false);
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("لا"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "المواد",
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),
          ),
        ),
        actions: [
          InkWell(
            child: Icon(Icons.close),
            onTap: (){
              yesNo_closeWS();
            },
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: Row(
          children: [
            Expanded(
              child: getListClasses(),
              flex: 1,
            ),
          ],
        ),
      ),
    );
  }


  Widget getListClasses(){
    return ListView.builder(
      itemCount: listArr.length,
      itemBuilder: (context, index) {
        return getListClassesItem(index);
      },
    );
  }
  Widget getListClassesItem(int index) {
    ClassesJsonModel model = listArr[index];
    Function onSendSection = (String data){
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => new Section(model,data)),
        );
    };
    return Container(
      color: Color(0xFF666666),
      margin: EdgeInsets.all(2.0),
      child: ListTile(
        title: Text(
          model.class_name,
          style: TextStyle(color: Colors.white, fontSize: 20),
          textAlign: TextAlign.center,
        ),
        onTap: (){
          Inc.ws.attendance.send.allSections(model.class_id,onSendSection);
        },
      ),
    );
  }


}
