import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sac/Inc/Inc.dart';
import 'package:sac/Screen/Classes.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Connection extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Connection();
  }
}

class _Connection extends State<Connection> {
  SharedPreferences _prefs;

  _iniSharedPreferences() async{
    _prefs = await SharedPreferences.getInstance();
  }

  String getServerIp(){
    try{
      String serverIp = _prefs.getString("serverIp");
      return serverIp;
    }catch(o){
      return "";
    }
  }

  String setServerIp(String serverIp){
    _prefs.setString("serverIp",serverIp);
  }

  int getServerPort(){
    try{
      int serverPort = _prefs.getInt("serverPort");
      return serverPort;
    }catch(o){
      return 8988;
    }
  }

  String setServerPort(int serverPort){
    _prefs.setInt("serverPort",serverPort);
  }

  _Connection(){
    _iniSharedPreferences();
  }

  serverPasswordError(){
    showDialog(
      context: context,
      builder:(BuildContext context){
        return AlertDialog(
          content: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Positioned(
                right: -40.0,
                top: -40.0,
                child: InkResponse(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: CircleAvatar(
                    child: Icon(Icons.close),
                    backgroundColor: Colors.red,
                  ),
                ),
              ),
              Form(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("كلمة المرور خاطئة"),
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("حسناً"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {

    TextEditingController txtIpAddress = TextEditingController();
    TextEditingController txtPort = TextEditingController();
    TextEditingController txtPassword = TextEditingController();

    txtIpAddress.text = getServerIp();
    txtPort.text = getServerPort().toString();

    Function onConnectAndGetClasses = (String data){
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => new Classes(data)),
        );
    };

    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              "الاتصال بالسيرفر",
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),
            ),
          ),
        ),
        body: Container(
          alignment: Alignment.center,
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.only(top: 5,bottom: 5),
                padding: EdgeInsets.only(top: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "عنوان الشبكة",
                      textDirection: TextDirection.rtl,
                    ),
                    TextField(
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.center,
                      controller: txtIpAddress,
                      keyboardType: TextInputType.url,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5,bottom: 5),
                padding: EdgeInsets.only(top: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "رقم المنفذ",
                      textDirection: TextDirection.rtl,
                    ),
                    TextField(
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.center,
                      controller: txtPort,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5,bottom: 5),
                padding: EdgeInsets.only(top: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "كلمة المرور",
                      textDirection: TextDirection.rtl,
                    ),
                    TextField(
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.center,
                      controller: txtPassword,
                      obscureText: true,
                    ),
                  ],
                ),
              ),
              RaisedButton(
                onPressed: () {
                  try{
                    String ipAddress = txtIpAddress.text;
                    setServerIp(ipAddress);
                    int port = int.parse(txtPort.text);
                    setServerPort(port);
                    String password = txtPassword.text;
                    Inc.ws.connectWithIp(ipAddress.trim(),port,password,onConnectAndGetClasses,serverPasswordError);
                  }catch(o){
                    print("o = "+o.toString());
                  }
                },
                child: Text(
                  "البدء",
                  textDirection: TextDirection.rtl,
                ),
              )
            ],
          ),
        ),
      );
  }
}
