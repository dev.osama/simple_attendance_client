import 'dart:collection';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sac/Inc/Inc.dart';
import 'package:sac/Screen/AttendanceDetails.dart';
import 'package:sac/Screen/Connection.dart';
import 'package:sac/WS-local/Model/ClassesJsonModel.dart';
import 'package:sac/WS-local/Model/SectionJsonModel.dart';
import 'package:sac/WS-local/Model/StudentsJsonModel.dart';

class Students extends StatefulWidget {
  String data = null;
  SectionJsonModel sectionJsonModel = null;
  ClassesJsonModel classesJsonMode = null;
  List<StudentsJsonModel> listArr = [];
  Students(this.classesJsonMode,this.sectionJsonModel,this.data){
    var jsonData = jsonDecode(data);
    for(var i in jsonData){
      listArr.add(StudentsJsonModel(i));
    }
  }
  @override
  State<StatefulWidget> createState() {
    return _Students(this.classesJsonMode,this.sectionJsonModel,this.listArr);
  }
}

class _Students extends State<Students> {
  List<StudentsJsonModel> listArr = null;
  SectionJsonModel sectionJsonModel = null;
  ClassesJsonModel classesJsonMode = null;
  HashMap<int,int> finalAttendsArr = HashMap();
  _Students(this.classesJsonMode,this.sectionJsonModel,this.listArr);

  yesNo_closeWS(){
    showDialog(
        context: context,
        builder:(BuildContext context){
          return AlertDialog(
            content: Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Positioned(
                  right: -40.0,
                  top: -40.0,
                  child: InkResponse(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: CircleAvatar(
                      child: Icon(Icons.close),
                      backgroundColor: Colors.red,
                    ),
                  ),
                ),
                Form(
//              key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("اغلاق الاتصال"),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("اغلاق الاتصال",textDirection: TextDirection.rtl,),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                child: Text("نعم"),
                                onPressed: () {
                                  Inc.ws.disconnect();
                                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => new Connection()), (route) => false);
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                child: Text("لا"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        },
    );
  }

  @override
  void initState() {
    super.initState();
    for(StudentsJsonModel itm in listArr){
      finalAttendsArr[itm.students_id] = 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "تحظير الطلاب",
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),
          ),
        ),
        actions: [
          InkWell(
            child: Icon(Icons.close),
            onTap: (){
              yesNo_closeWS();
            },
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Expanded(
              child: getListStudents(),
              flex: 5,
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                child: RaisedButton(
                  child: Text("التالي"),
                  onPressed: (){
                    Navigator.push(
                      context,
                      new MaterialPageRoute(builder: (context) => new AttendanceDetails(this.classesJsonMode,this.sectionJsonModel,this.finalAttendsArr)),
                    );
                  },
                ),
              ),
              flex: 1,
            ),
          ],
        ),
      ),
    );
  }

  setA(int students_id,int value){
    setState(() {
      finalAttendsArr[students_id] = value;
    });
  }


  Widget getListStudents(){
    return ListView.builder(
      itemCount: listArr.length,
      itemBuilder: (context, index) {
        return getListStudentsItem(index);
      },
    );
  }

  Widget getListStudentsItem(int index) {
    StudentsJsonModel model = listArr[index];

    //present = 1
    //late    = 2
    //absent  = 3

    // int value = 1;
    return Container(
      child: Column(
        children: [
          ListTile(
            title: Container(
              color: Color(0xFF666666),
              child: Text(
                model.students_name,
                style: TextStyle(color: Colors.white, fontSize: 26,),
                textAlign: TextAlign.center,
              ),
            ),
            subtitle: Container(
              width: double.infinity,
              margin: EdgeInsets.all(0),
              child: Row(
                textDirection: TextDirection.rtl,
                children: [
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Text("حاظر"),
                        Radio(
                          // groupValue: selectedGroup,
                          groupValue: finalAttendsArr[model.students_id],
                          value: 1,
                          activeColor: Colors.green,
                          onChanged: (int v){
                            setA(model.students_id, v);
                          },
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Text("متأخر"),
                        Radio(
                          activeColor: Colors.orangeAccent,
                          groupValue: finalAttendsArr[model.students_id],
                          value: 2,
                          onChanged: (int v){
                            setA(model.students_id, v);
                          },
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Text("غائب"),
                        Radio(
                          activeColor: Colors.red,
                          groupValue: finalAttendsArr[model.students_id],
                          value: 3,
                          onChanged: (int v){
                            setA(model.students_id, v);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Divider(
            height: 5,
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}