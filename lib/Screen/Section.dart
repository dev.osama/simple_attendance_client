import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sac/Inc/Inc.dart';
import 'package:sac/Screen/Connection.dart';
import 'package:sac/Screen/SectionPassword.dart';
import 'package:sac/WS-local/Model/ClassesJsonModel.dart';
import 'package:sac/WS-local/Model/SectionJsonModel.dart';

class Section extends StatefulWidget {
  String data = null;
  List<SectionJsonModel> listArr = [];
  ClassesJsonModel classesJsonModel = null;
  Section(this.classesJsonModel ,this.data){
    var jsonData = jsonDecode(data);
    for(var i in jsonData){
      listArr.add(SectionJsonModel(i));
    }
  }
  @override
  State<StatefulWidget> createState() {
    return _Section(this.classesJsonModel,this.listArr);
  }
}

class _Section extends State<Section> {
  List<SectionJsonModel> listArr;
  ClassesJsonModel classesJsonModel = null;
  _Section(this.classesJsonModel,this.listArr);
  yesNo_closeWS(){
    showDialog(
      context: context,
      builder:(BuildContext context){
        return AlertDialog(
          content: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Positioned(
                right: -40.0,
                top: -40.0,
                child: InkResponse(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: CircleAvatar(
                    child: Icon(Icons.close),
                    backgroundColor: Colors.red,
                  ),
                ),
              ),
              Form(
//              key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("اغلاق الاتصال"),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("اغلاق الاتصال",textDirection: TextDirection.rtl,),
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("نعم"),
                              onPressed: () {
                                Inc.ws.disconnect();
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => new Connection()), (route) => false);
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("لا"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "الشعب",
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),
          ),
        ),
        actions: [
          InkWell(
            child: Icon(Icons.close),
            onTap: (){
              yesNo_closeWS();
            },
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: Row(
          children: [
            Expanded(
              child: getListSection(),
              flex: 1,
            )
          ],
        ),
      ),
    );
  }


  Widget getListSection(){
    return ListView.builder(
      itemCount: listArr.length,
      itemBuilder: (context, index) {
        return getListSectionItem(index);
      },
    );
  }
  Widget getListSectionItem(int index) {
    SectionJsonModel model = listArr[index];
    return Container(
      color: Color(0xFF666666),
      margin: EdgeInsets.all(2.0),
      child: ListTile(
        title: Text(
          model.section_name,
          style: TextStyle(color: Colors.white, fontSize: 20),
          textAlign: TextAlign.center,
        ),
        onTap: (){
          Navigator.push(
            context,
            new MaterialPageRoute(builder: (context) => new SectionPassword(this.classesJsonModel,model)),
          );
        },
      ),
    );
  }
}
